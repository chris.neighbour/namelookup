# Name Lookup

A small rest service that allows the storage of **names** along with a corresponding **postcode** which can then be searched later. This service is temporarily being hosted with a Ubuntu compute instance on the Google Cloud Platform and can be reached at `http://namelookup.neighbour.dev`.

## Storing Names & Postcodes

You can store names & postcodes into this service by sending a `POST` request to `/name` of the server. The format should be in a JSON array with the child object's containing the respective **name** and **postcode** attributes. For example:

```json
[
	{ "name": "Richard Hendricks", "postcode": 3433 },
	{ "name": "Erlich Bachman", "postcode": 6045 },
	{ "name": "Bertram Gilfoyle", "postcode": 6033 },
	{ "name": "Dinesh Chugtai", "postcode": 6412 },
	{ "name": "Donald 'Jared' Dunn", "postcode": 6214 },
	{ "name": "Jian-Yang!", "postcode": 6522 },
	{ "name": "Nelson Bighetti", "postcode": 6243 },
	{ "name": "Monica Hall", "postcode": 6274 }
]
```

Postcodes are expected to be formatted according to the [Australia Postal Service](https://postcode.auspost.com.au/free_display.html?id=1) (ranging from `0200` (0200 ANU) to `9999` (North Pole). Any postcodes outside this range or contain invalid characters will be rejected.

Names are also to be between 2 and 32 characters long (would have done longer but made the tests look bulky).

## Fetching Names by Postcode Range

The names, and a sum of all characters, can be retrieved by `GET` request to the `/name` endpoint of the server along with a filtered postcode range placed in the `postcodeStart` and `postcodeEnd` query parameters.

```sh
$curl -L -X GET 'localhost:8080/?postcodeStart=3433&postcodeEnd=6100'
```

This should return for the prior dataset

```json
{
    "names": [
        "Bertram Gilfoyle",
        "Erlich Bachman",
        "Richard Hendricks"
    ],
    "charCount": 47
}
```