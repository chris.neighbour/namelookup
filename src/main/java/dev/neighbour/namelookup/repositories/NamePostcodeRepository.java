package dev.neighbour.namelookup.repositories;

import dev.neighbour.namelookup.models.NamePostcode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NamePostcodeRepository extends CrudRepository<NamePostcode, Integer> {
    List<NamePostcode> findByPostcodeBetween(int min, int max);
}
