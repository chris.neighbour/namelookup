package dev.neighbour.namelookup.services;

import dev.neighbour.namelookup.models.NamePostcodeDTO;
import dev.neighbour.namelookup.models.NamePostcode;
import dev.neighbour.namelookup.models.NamesListDTO;
import dev.neighbour.namelookup.repositories.NamePostcodeRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NamePostcodeService {

    @Autowired
    NamePostcodeRepository namePostcodeRepository;

    public Iterable<NamePostcode> saveListOfNamePostcodeDTOs(List<NamePostcodeDTO> postcodeDTOs) {
        return namePostcodeRepository.saveAll(postcodeDTOs
                .stream()
                .map(NamePostcodeDTO::toModel)
                .collect(Collectors.toList())
        );
    }

    public NamesListDTO fetchNamesListDTOByPostcodeRange(int start, int end) {
        List<String> names = namePostcodeRepository.findByPostcodeBetween(start, end).stream()
                .sorted(Comparator.comparing(NamePostcode::getName))
                .map(NamePostcode::getName)
                .collect(Collectors.toList());

        int charCount = names.stream()
                .map(String::length)
                .reduce(0,  Integer::sum);

        return new NamesListDTO(names, charCount);
    }
}
