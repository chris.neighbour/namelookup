package dev.neighbour.namelookup.controllers;

import dev.neighbour.namelookup.exceptions.InvalidNameException;
import dev.neighbour.namelookup.exceptions.InvalidPostcodeException;
import dev.neighbour.namelookup.models.ExceptionDTO;
import dev.neighbour.namelookup.models.NamesListDTO;
import dev.neighbour.namelookup.models.NamePostcodeDTO;
import dev.neighbour.namelookup.models.NamePostcode;
import dev.neighbour.namelookup.services.NamePostcodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

@RestController
@Validated
public class NamePostcodeController {

    @Autowired
    NamePostcodeService namePostcodeService;

    private static final Logger logger = LogManager.getLogger(NamePostcodeController.class);

    @PostMapping(value = "/name")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void storeListOfPostcodeNames(@RequestBody List<@Valid NamePostcodeDTO> postcodeDTOs) {
        namePostcodeService.saveListOfNamePostcodeDTOs(postcodeDTOs);
    }

    @GetMapping("/name")
    @ResponseStatus(HttpStatus.OK)
    public NamesListDTO fetchNamesListByPostcodeRange(@RequestParam(value = "postcodeStart") String postcodeStart, @RequestParam(value = "postcodeEnd") String postcodeEnd) {
        return namePostcodeService.fetchNamesListDTOByPostcodeRange(NamePostcode.ParsePostcode(postcodeStart), NamePostcode.ParsePostcode(postcodeEnd));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ExceptionDTO handleException(HttpServletRequest req, ConstraintViolationException exception) {
        logger.info(String.format("ConstraintViolationException: %s %s", req.getRequestURL(), exception));
        return new ExceptionDTO("Error processing you request", exception.getMessage());
    }

    @ExceptionHandler({InvalidNameException.class, InvalidPostcodeException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ExceptionDTO handleException(HttpServletRequest req, Exception exception) {
        logger.info(String.format("InvalidDataException: %s %s", req.getRequestURL(), exception));
        return new ExceptionDTO("Error processing you request", exception.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ExceptionDTO handleException(HttpServletRequest req, HttpMessageNotReadableException exception) {
        logger.info(String.format("HttpMessageNotReadableException: %s %s", req.getRequestURL(), exception));
        return new ExceptionDTO("Error processing you request", "JSON body doesn't conform to request schema");
    }

}
