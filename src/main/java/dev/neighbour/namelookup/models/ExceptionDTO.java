package dev.neighbour.namelookup.models;

public class ExceptionDTO {

    private final String message;
    private final String error;

    public ExceptionDTO(String message, String error) {
        this.message = message;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }
}
