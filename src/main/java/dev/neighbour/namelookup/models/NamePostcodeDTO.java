package dev.neighbour.namelookup.models;

import javax.validation.constraints.*;

public class NamePostcodeDTO {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 32, message = "name must be between 2 and 32 characters long")
    private final String name;

    @NotNull
    @Min(value = 200, message = "postcode must be between 200 & 9999")
    @Max(value = 99999, message = "postcode must be between 200 & 9999")
    private final int postcode;

    public NamePostcodeDTO(String name, int postcode) {
        this.name = name;
        this.postcode = postcode;
    }

    public NamePostcode toModel() {
        return new NamePostcode(this.name, this.postcode);
    }
}
