package dev.neighbour.namelookup.models;

import java.util.List;

public class NamesListDTO {

    private final List<String> names;
    private final int charCount;

    public NamesListDTO(List<String> names, int charCount) {
        this.names = names;
        this.charCount = charCount;
    }

    public List<String> getNames() {
        return names;
    }

    public int getCharCount() {
        return charCount;
    }
}
