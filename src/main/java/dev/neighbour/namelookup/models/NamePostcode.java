package dev.neighbour.namelookup.models;

import dev.neighbour.namelookup.exceptions.InvalidNameException;
import dev.neighbour.namelookup.exceptions.InvalidPostcodeException;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Table(name = "name_postcodes")
public class NamePostcode {

    public static String ParseName(String name) throws InvalidNameException {
        if (name == null || name.length() < 2 || name.length() > 32) {
            throw new InvalidNameException("name must be between 2 and 32 characters long");
        }

        return name;
    }

    public static int ParsePostcode(String postcode) {
        if (!StringUtils.isNumeric(postcode)) {
            throw new InvalidPostcodeException("postcode must be between 200 & 9999");
        }

        return NamePostcode.ParsePostcode(Integer.parseInt(postcode));
    }

    public static int ParsePostcode(int postcode) {
        if (postcode < 200 || postcode > 9999) {
            throw new InvalidPostcodeException("postcode must be between 200 & 9999");
        }

        return postcode;
    }

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column( nullable = false)
    private String name;

    @Column(nullable = false)
    private int postcode;

    public NamePostcode() {

    }

    public NamePostcode(String name, int postcode) {
        setName(name);
        setPostcode(postcode);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = NamePostcode.ParseName(name);
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = NamePostcode.ParsePostcode(postcode);
    }
}
