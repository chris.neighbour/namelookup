package dev.neighbour.namelookup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameLookupApplication {

	public static void main(String[] args) {
		SpringApplication.run(NameLookupApplication.class, args);
	}

}
