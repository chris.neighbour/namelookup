package dev.neighbour.namelookup.models;

import dev.neighbour.namelookup.exceptions.InvalidNameException;
import dev.neighbour.namelookup.exceptions.InvalidPostcodeException;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.assertj.core.api.Assertions.assertThat;

public class NamePostcodeTests {

    @Test
    public void shouldParsePostCode() throws Exception {
        int postcode = NamePostcode.ParsePostcode("6000");
        assertThat(postcode).isEqualTo(6000);
    }

    @Test
    public void shouldErrorParsingNullPostCode() throws Exception {
        Exception exception = assertThrows(InvalidPostcodeException.class, () -> {
            NamePostcode.ParsePostcode(null);
        });
        assertTrue(exception.getMessage().contains("postcode must be between 200 & 9999"));
    }

    @Test
    public void shouldErrorParsingEmptyPostCode() throws Exception {
        Exception exception = assertThrows(InvalidPostcodeException.class, () -> {
            NamePostcode.ParsePostcode("");
        });
        assertTrue(exception.getMessage().contains("postcode must be between 200 & 9999"));
    }

    @Test
    public void shouldErrorParsingNonNumericPostCode() throws Exception {
        Exception exception = assertThrows(InvalidPostcodeException.class, () -> {
            NamePostcode.ParsePostcode("aabbv");
        });
        assertTrue(exception.getMessage().contains("postcode must be between 200 & 9999"));
    }

    @Test
    public void shouldErrorParsingPostCodeBelow200() throws Exception {
        Exception exception = assertThrows(InvalidPostcodeException.class, () -> {
            NamePostcode.ParsePostcode("199");
        });
        assertTrue(exception.getMessage().contains("postcode must be between 200 & 9999"));
    }

    @Test
    public void shouldErrorParsingPostCodeAbove9999() throws Exception {
        Exception exception = assertThrows(InvalidPostcodeException.class, () -> {
            NamePostcode.ParsePostcode("10000");
        });
        assertTrue(exception.getMessage().contains("postcode must be between 200 & 9999"));
    }

    @Test
    public void shouldParseName() throws Exception {
        String name = NamePostcode.ParseName("Charlie");
        assertThat(name).isEqualTo("Charlie");
    }

    @Test
    public void shouldErrorParsingNullName() throws Exception {
        Exception exception = assertThrows(InvalidNameException.class, () -> {
            NamePostcode.ParseName(null);
        });
        assertTrue(exception.getMessage().contains("name must be between 2 and 32 characters long"));
    }

    @Test
    public void shouldErrorParsingEmptyName() throws Exception {
        Exception exception = assertThrows(InvalidNameException.class, () -> {
            NamePostcode.ParseName("");
        });
        assertTrue(exception.getMessage().contains("name must be between 2 and 32 characters long"));
    }

    @Test
    public void shouldErrorParsingTooShortName() throws Exception {
        Exception exception = assertThrows(InvalidNameException.class, () -> {
            NamePostcode.ParseName("a");
        });
        assertTrue(exception.getMessage().contains("name must be between 2 and 32 characters long"));
    }

    @Test
    public void shouldErrorParsingTooLongName() throws Exception {
        Exception exception = assertThrows(InvalidNameException.class, () -> {
            NamePostcode.ParseName("fhslehgnfjreslengitoensldjfutiomr");
        });
        assertTrue(exception.getMessage().contains("name must be between 2 and 32 characters long"));
    }
}
