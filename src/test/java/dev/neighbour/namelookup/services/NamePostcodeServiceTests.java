package dev.neighbour.namelookup.services;

import dev.neighbour.namelookup.models.NamePostcode;
import dev.neighbour.namelookup.models.NamePostcodeDTO;

import dev.neighbour.namelookup.models.NamesListDTO;
import dev.neighbour.namelookup.repositories.NamePostcodeRepository;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
public class NamePostcodeServiceTests {

    @MockBean
    private NamePostcodeRepository repository;

    @Autowired
    NamePostcodeService namePostcodeService;

    @Test
    public void shouldStoreEmptyList() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        namePostcodeService.saveListOfNamePostcodeDTOs(new ArrayList<NamePostcodeDTO>());

        verify(repository, times(1)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldStoreListOfNamePostcodeDTOs() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        namePostcodeService.saveListOfNamePostcodeDTOs(Arrays.asList(new NamePostcodeDTO("Charlie", 6382), new NamePostcodeDTO("Eric Bachman", 6581)));

        verify(repository, times(1)).saveAll(Mockito.any(ArrayList.class));
        verify(repository).saveAll(argThat(
                (namePostcodes)-> {
                    namePostcodes.forEach(namePostcode -> {
                        assertTrue((namePostcode.getName().equals("Charlie") && namePostcode.getPostcode() == 6382) ||
                                (namePostcode.getName().equals("Eric Bachman") && namePostcode.getPostcode() == 6581)
                        );
                    });
                    return true;
                }
        ));
    }

    @Test
    public void shouldReturnEmptyNameList() throws Exception {
        when(repository.findByPostcodeBetween(6100, 6200))
                .thenReturn(Arrays.asList(
                        new NamePostcode("Monica Hall", 6123),
                        new NamePostcode("Erlich Bachman", 6140),
                        new NamePostcode("Bertram Gilfoyle", 6190)
                ));

        NamesListDTO responseList = namePostcodeService.fetchNamesListDTOByPostcodeRange(6100, 6200);

        assertTrue(responseList.getNames().equals(Arrays.asList("Bertram Gilfoyle", "Erlich Bachman", "Monica Hall")));
        assertTrue(responseList.getCharCount() == 41);

        verify(repository, times(1)).findByPostcodeBetween(6100, 6200);

    }


}
