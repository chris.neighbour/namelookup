package dev.neighbour.namelookup.controllers;

import dev.neighbour.namelookup.models.NamePostcode;
import dev.neighbour.namelookup.repositories.NamePostcodeRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class NamePostcodeControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NamePostcodeRepository repository;

    @Test
    public void shouldStoreEmptyArray() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isCreated());

        verify(repository, times(1)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldStoreArrayOfNames() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Charlie\", \"postcode\": 6013 }, { \"name\": \"Richard Hendricks\", \"postcode\": 6125 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isCreated());


        verify(repository, times(1)).saveAll(Mockito.any(ArrayList.class));
        verify(repository).saveAll(argThat(
                (namePostcodes)-> {
                    namePostcodes.forEach(namePostcode -> {
                        assertTrue((namePostcode.getName().equals("Charlie") && namePostcode.getPostcode() == 6013) ||
                                (namePostcode.getName().equals("Richard Hendricks") && namePostcode.getPostcode() == 6125)
                        );
                    });
                    return true;
                }
        ));
    }

    @Test
    public void shouldErrorWithMissingName() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be empty")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithNullName() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": null, \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be null")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithEmptyName() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"\", \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("must not be empty")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithTooShortName() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"c\", \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("name must be between 2 and 32 characters long")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithTooLongName() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"dhfjrnghtidjskwifoentorowlsmcjfod\", \"postcode\": 6000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("name must be between 2 and 32 characters long")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithMissingPostcode() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Charlie\" }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithPostcodeBelowRange() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Charlie\", \"postcode\": 199 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithPostcodeAboveRange() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Charlie\", \"postcode\": 10000 }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldErrorWithInvalidPostcodeType() throws Exception {
        when(repository.saveAll(Mockito.any(ArrayList.class)))
                .thenAnswer(request -> request.getArguments()[0]);

        MockHttpServletRequestBuilder request = post("/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{ \"name\": \"Charlie\", \"postcode\": \"Cheeky\" }]");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("JSON body doesn't conform to request schema")));

        verify(repository, times(0)).saveAll(Mockito.any(ArrayList.class));
    }

    @Test
    public void shouldFetchWithResponse() throws Exception {
        when(repository.findByPostcodeBetween(200, 9999)).thenReturn( Arrays.asList(new NamePostcode("Charlie", 3920),
                new NamePostcode("Sean", 6584)));

        MockHttpServletRequestBuilder request = get("/name?postcodeStart=200&postcodeEnd=9999")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"names\":[\"Charlie\",\"Sean\"],\"charCount\":11}")));

        verify(repository, times(1)).findByPostcodeBetween(200, 9999);
    }

    @Test
    public void shouldFetchWithEmptyResponse() throws Exception {
        when(repository.findByPostcodeBetween(201, 9998)).thenReturn( Arrays.asList());

        MockHttpServletRequestBuilder request = get("/name?postcodeStart=201&postcodeEnd=9998")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"names\":[],\"charCount\":0}")));

        verify(repository, times(1)).findByPostcodeBetween(201, 9998);
    }

    @Test
    public void shouldErrorWithLowerBoundStart() throws Exception {
        MockHttpServletRequestBuilder request = get("/name?postcodeStart=199&postcodeEnd=9999")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLUpperBoundStart() throws Exception {
        MockHttpServletRequestBuilder request = get("/name?postcodeStart=10000&postcodeEnd=9999")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLowerBoundEnd() throws Exception {
        MockHttpServletRequestBuilder request = get("/name?postcodeStart=200&postcodeEnd=199")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }

    @Test
    public void shouldErrorWithLUpperBoundEnd() throws Exception {
        MockHttpServletRequestBuilder request = get("/name?postcodeStart=200&postcodeEnd=10000")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("postcode must be between 200 & 9999")));
    }
}
