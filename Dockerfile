FROM openjdk:11
WORKDIR /
ADD target/namelookup-0.0.1-SNAPSHOT.jar namelookup.jar
EXPOSE 8080
CMD java -jar namelookup.jar